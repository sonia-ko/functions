const getSum = (str1, str2) => {
    // add your implementation below
    let a = Number(str1);
    let b = Number(str2);
    if (
        typeof str1 !== 'string' ||
        typeof str2 !== 'string' ||
        isNaN(a) ||
        isNaN(b)
    ) {
        return false;
    }

    return (a + b).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    // add your implementation below
    let posts = 0;
    let comments = 0;
    for (const item of listOfPosts) {
        if (item.author === authorName) {
            posts++;
        }

        if (item.comments) {
            for (const comment of item.comments) {
                if (comment.author === authorName) {
                    comments++;
                }
            }
        }
    }

    return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
    // add your implementation below
    const ticketPrice = 25;
    let amount = 0;
    let moneyAvailable = [];
    let answer = 'NO';

    for (let i = 0; i < people.length; i++) {
        if (+people[i] === ticketPrice) {
            answer = 'YES';
            moneyAvailable.push(ticketPrice);
            amount += 25;
        } else {
            const amountNeeded = +people[i] - ticketPrice;
            if (amount < amountNeeded) {
                answer = 'NO';
                break;
            } else {
                if (amountNeeded === 25) {
                    if (moneyAvailable.includes(25)) {
                        moneyAvailable.push(+people[i]);
                        const index = moneyAvailable.indexOf(25);
                        moneyAvailable.splice(index, 1);
                        amount = amount - 25 + +people[i];
                    } else {
                        answer = 'NO';
                        break;
                    }
                }
                if (amountNeeded === 50) {
                    if (moneyAvailable.includes(50)) {
                        moneyAvailable.push(+people[i]);
                        const index = moneyAvailable.indexOf(50);
                        moneyAvailable.splice(index, 1);
                        amount = amount - 50 + +people[i];
                        answer = 'YES';
                    } else if (moneyAvailable.includes(25)) {
                        if (moneyAvailable.includes(25)) {
                            moneyAvailable.push(+people[i]);
                            const index1 = moneyAvailable.indexOf(25);
                            moneyAvailable.splice(index1, 1);
                            const index2 = moneyAvailable.indexOf(25);
                            moneyAvailable.splice(index2, 1);
                            amount = amount - 50 + +people[i];
                            answer = 'YES';
                        } else {
                            answer = 'NO';
                            break;
                        }
                    } else {
                        answer = 'NO';
                        break;
                    }
                }
                if (amountNeeded === 75) {
                    if (moneyAvailable.includes(50) && moneyAvailable.includes(25)) {
                        moneyAvailable.push(+people[i]);
                        let index = moneyAvailable.indexOf(50);
                        moneyAvailable.splice(index, 1);
                        const index2 = moneyAvailable.indexOf(25);
                        moneyAvailable.splice(index2, 1);
                        amount = amount - 75 + +people[i];
                        answer = 'YES';
                    } else if (moneyAvailable.includes(25)) {
                        if (moneyAvailable.includes(25)) {
                            if (moneyAvailable.includes(25)) {
                                moneyAvailable.push(+people[i]);
                                const index1 = moneyAvailable.indexOf(25);
                                moneyAvailable.splice(index1, 1);
                                const index2 = moneyAvailable.indexOf(25);
                                moneyAvailable.splice(index2, 1);
                                const index3 = moneyAvailable.indexOf(25);
                                moneyAvailable.splice(index3, 1);
                                amount = amount - 75 + +people[i];
                                answer = 'YES';
                            } else {
                                answer = 'NO';
                                break;
                            }
                        } else {
                            answer = 'NO';
                            break;
                        }
                    }
                }
            }
        }
    }

    return answer;
};

// 25 50 100 // no

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
